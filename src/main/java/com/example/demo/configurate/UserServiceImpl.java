package com.example.demo.configurate;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Override
    @Async
    public void writeText(int i) {
        System.out.println("线程"+i+","+Thread.currentThread().getName()+"执行异步任务");
    }
}
