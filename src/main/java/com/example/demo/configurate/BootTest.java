package com.example.demo.configurate;

import com.example.demo.celue.Food;
import com.example.demo.celue.FoodCeNue;
import javafx.application.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BootTest {
     @Autowired
     private RedisTemplate redisTemplate;

    @Autowired
    AsyncController asyncController;
    @Autowired
    private FoodCeNue foodCeNue;

    @Test
    public void test(){
//        redisTemplate.opsForValue().set("student", "kirito");
//        asyncController.learnAysnc();
//        String value= (String) redisTemplate.opsForValue().get("student");
//        String value2= (String) redisTemplate.opsForValue().get("redisKey");
//        System.out.println(value2);
        Food food=foodCeNue.getFoodByType("fruit");
        Food food1=foodCeNue.getFoodByType("rice");
        food.name();
        food1.name();
    }


    public boolean lock(String key,Long seconds){
        return (Boolean) redisTemplate.execute((RedisCallback) connection -> {
            /** 如果不存在,那么则true,则允许执行, */
            Boolean acquire = connection.setNX(key.getBytes(),
                    String.valueOf(key).getBytes());
            /** 防止死锁,将其key设置过期时间 */
            connection.expire(key.getBytes(), seconds);
            if (acquire) {
                return true;
            }
            return false;
        });
    }



    /**
     * 删除锁
     * @param key
     */
    public void delete(String key) {
        redisTemplate.delete(key);
    }

}
