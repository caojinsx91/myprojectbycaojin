package com.example.demo.configurate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Duration;

@RestController
@RequestMapping("/async/test")
public class AsyncController {
    @Autowired
   private UserService service;
    @Autowired
    private RedisTemplate redisTemplate;
   // @Async
    public void learnAysnc(){
        for(int i=0;i<10;i++){
            service.writeText(i);
        }
    }
    private final String key="redisKey";
    private final Long seconds=15000L;


    public boolean lock(String key,Long seconds){

        boolean acquire=redisTemplate.opsForValue().
                setIfAbsent(key,  Duration.ofSeconds(10));
            if (acquire) {
                return true;
            }
            return false;

    }



    /**
     * 删除锁
     * @param key
     */
    public void delete(String key) {
        redisTemplate.delete(key);
    }


    @GetMapping("/testRedis")
    public String testRedisData() throws InterruptedException {
        boolean lock=lock(key,seconds);
        System.out.println(lock);
        if(!lock){
            return "其它业务正在进行处理，请稍后";
        }else{
            //休眠30秒
            Thread.sleep(seconds);
            String s1="执行正确!";
            delete(key);
           return s1;
        }
    }
}
