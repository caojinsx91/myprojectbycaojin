package com.example.demo.celue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class FoodCeNue {
    @Autowired
    private Map<String,Food> food;

    public Food getFoodByType(String type) {
        System.out.println(food.get(type));
        return food.get(type);
    }

}
