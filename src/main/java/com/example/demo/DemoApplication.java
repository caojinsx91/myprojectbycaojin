package com.example.demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
@EnableAsync
@MapperScan("com.example.demo.celue.mapper")
@EnableDiscoveryClient
public class DemoApplication {

	@GetMapping("/home")
	String home() {
		return "Spring is here!";
	}

	public static void main(String[] args) {
		
		SpringApplication.run(DemoApplication.class, args);
	}
}